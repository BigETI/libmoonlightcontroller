#ifndef __MOONLIGHT_CONTROLLER_E_XINPUT_BATTERY_DEVICE_TYPE_H__
#	define __MOONLIGHT_CONTROLLER_E_XINPUT_BATTERY_DEVICE_TYPE_H__

// Moonlight controller namespace
namespace MoonlightController
{
	// XInput battery device type enumerator
	enum EXInputBatteryDeviceType
	{
		// Gamepad
		EXInputBatteryDeviceType_Gamepad = 0x0,

		// Headset
		EXInputBatteryDeviceType_Headset = 0x1
	};
}
#endif
