#ifndef __MOONLIGHT_CONTROLLER_E_USER_INPUT_OUTPUT_TYPE_H__
#   define __MOONLIGHT_CONTROLLER_E_USER_INPUT_OUTPUT_TYPE_H__

// Moonlight controller namespace
namespace MoonlightController
{
    // User input output type enumerator
    enum EUserInputOutputType
    {
        // Keyboard
        EUserInputOutputType_Keyboard,

        // Mouse
        EUserInputOutputType_Mouse,

        // Gamepad
        EUserInputOutputType_Gamepad
    };
}
#endif
