#ifndef __MOONLIGHT_CONTROLLER_E_MOUSE_BUTTON_H__
#	define __MOONLIGHT_CONTROLLER_E_MOUSE_BUTTON_H__

// Moonlight controller namespace
namespace MoonlightController
{
	// Mouse button enumerator
	enum EMouseButton
	{
		// Left
		EMouseButton_Left,

		// Right
		EMouseButton_Right,

		// Middle
		EMouseButton_Middle
	};
}
#endif
