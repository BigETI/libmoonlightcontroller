#ifndef __MOONLIGHT_CONTROLLER_E_INPUT_AXIS_H__
#	define __MOONLIGHT_CONTROLLER_E_INPUT_AXIS_H__

// Moonlight controller namespace
namespace MoonlightController
{
	// XInput axis enumerator
	enum EXInputAxis
	{
		// Thumb LX
		EXInputAxis_ThumbLX,

		// Thumb LY
		EXInputAxis_ThumbLY,

		// Thumb RX
		EXInputAxis_ThumbRX,

		// Thumb RY
		EXInputAxis_ThumbRY,

		// Left trigger
		EXInputAxis_LeftTrigger,

		// Right trigger
		EXInputAxis_RightTrigger
	};
}
#endif
