#ifndef __MOONLIGHT_CONTROLLER_E_XINPUT_DEVICE_TYPE_H__
#	define __MOONLIGHT_CONTROLLER_E_XINPUT_DEVICE_TYPE_H__

// Moonlight controller namespace
namespace MoonlightController
{
	// XInput device type enumerator
	enum EXInputDeviceType
	{
		// Gamepad
		EXInputDeviceType_Gamepad = 0x1
	};
}
#endif
