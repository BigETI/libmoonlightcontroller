# Moonlight controller

## Description
Moonlight controller is a cross platform library to control user I/O interfaces using a scripting backend.

## Supported languages
- C++
- Lua

## Supported platforms
- Linux
- OSX
- Windows

## Licence
The [licence](https://github.com/BigETI/libmoonlightcontroller/blob/master/LICENCE.md) can be found here: [https://github.com/BigETI/libmoonlightcontroller/blob/master/LICENCE.md](https://github.com/BigETI/libmoonlightcontroller/blob/master/LICENCE.md)

## Documentation
The [documentation](https://github.com/BigETI/libmoonlightcontroller/blob/master/DOCUMENTATION.md) can be found here: [https://github.com/BigETI/libmoonlightcontroller/blob/master/DOCUMENTATION.md](https://github.com/BigETI/libmoonlightcontroller/blob/master/DOCUMENTATION.md)

## Important notes
This project is still in development. Code for all platforms are not implemented yet.

## How to contribute to this project?
Anything can be changed to contribute. Create a fork from this project, apply your changes and create a pull request.