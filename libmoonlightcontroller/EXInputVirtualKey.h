#ifndef __MOONLIGHT_CONTROLLER_E_XINPUT_VIRTUAL_KEY_H__
#	define __MOONLIGHT_CONTROLLER_E_XINPUT_VIRTUAL_KEY_H__

// Moonlight controller namespace
namespace MoonlightController
{
	// XInput virtual key enumerator
	enum EXInputVirtualKey
	{
		// A
		EXInputVirtualKey_A = 0x5800,

		// B
		EXInputVirtualKey_B = 0x5801,

		// X
		EXInputVirtualKey_X = 0x5802,

		// Y
		EXInputVirtualKey_Y = 0x5803,

		// Right shoulder
		EXInputVirtualKey_RightShoulder = 0x5804,

		// Left shoulder
		EXInputVirtualKey_LeftShoulder = 0x5805,

		// Left trigger
		EXInputVirtualKey_LeftTrigger = 0x5806,

		// Right trigger
		EXInputVirtualKey_RightTrigger = 0x5807,

		// D-Pad up
		EXInputVirtualKey_DPadUp = 0x5810,

		// D-Pad down
		EXInputVirtualKey_DPadDown = 0x5811,

		// D-Pad left
		EXInputVirtualKey_DPadLeft = 0x5812,

		// D-Pad right
		EXInputVirtualKey_DPadRight = 0x5813,

		// Start
		EXInputVirtualKey_Start = 0x5814,

		// Back
		EXInputVirtualKey_Back = 0x5815,

		// Left thumb press
		EXInputVirtualKey_LeftThumbPress = 0x5816,

		// Right thumb press
		EXInputVirtualKey_RightThumbPress = 0x5817,

		// Left thumb up
		EXInputVirtualKey_LeftThumbUp = 0x5820,

		// Left thumb down
		EXInputVirtualKey_LeftThumbDown = 0x5821,

		// Left thumb right
		EXInputVirtualKey_LeftThumbRight = 0x5822,

		// Left thumb left
		EXInputVirtualKey_LeftThumbLeft = 0x5823,

		// Left thumb up-left
		EXInputVirtualKey_LeftThumbUpLeft = 0x5824,

		// Left thumb upright
		EXInputVirtualKey_LeftThumbUpRight = 0x5825,

		// Left thumb down-right
		EXInputVirtualKey_LeftThumbDownRight = 0x2826,

		// Left thumb down-left
		EXInputVirtualKey_LeftThumbDownLeft = 0x5827,

		// Right thumb up
		EXInputVirtualKey_RightThumbUp = 0x5830,

		// Right thumb down
		EXInputVirtualKey_RightThumbDown = 0x5831,

		// Right thumb right
		EXInputVirtualKey_RightThumbRight = 0x5832,

		// Right thumb left
		EXInputVirtualKey_RightThumbLeft = 0x5833,

		// Right thumb up-left
		EXInputVirtualKey_RightThumbUpLeft = 0x5834,

		// Right thumb up-right
		EXInputVirtualKey_RightThumbUpRight = 0x5835,

		// Right thumb down-right
		EXInputVirtualKey_RightThumbDownRight = 0x5836,

		// Right thumb down-left
		EXInputVirtualKey_RightThumbDownLeft = 0x5837
	};
}
#endif
